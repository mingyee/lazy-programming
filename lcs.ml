open Memoizer
open Timing

type base = Base.base;;
type dna = Base.dna;;

(* slow lcs *)
let rec slow_lcs ((s1,s2) : dna * dna) : dna =
  match (s1,s2) with 
  | ([], _) -> []
  | (_, []) -> []
  | (x :: xs, y :: ys) ->
      if Base.eq x y then
        x :: slow_lcs (xs, ys)
      else
        Base.longer_dna_of (slow_lcs (s1, ys)) (slow_lcs (xs, s2))
;;

(* A potentially useful module *)
module DnaPairOrder : Map.OrderedType with type t = dna * dna =
struct
    type t = dna * dna

    let rec compare_dna x' y' : int = 
      match x',y' with 
      | [], [] -> 0
      | [], xs -> -1
      | xs, [] -> 1
      | x::xs, y::ys -> (
	        match Base.compare x y with
	        | 0     -> compare_dna xs ys
          | other -> other
	      )

    (* implements a lexicographic ordering: 
     * compare the second components only if first components are equal *)
    let compare (a, b) (c, d) =
      match compare_dna a c with
	    | 0     -> compare_dna b d
      | other -> other
     
end;;


(* Task 4.4 *)

(* implement fast_lcs using your automatic memoizer functor! 
 * doing so will of course require proper creation of modules and
 * use of functors *)
module AutoMemo = Memoizer(Map.Make(DnaPairOrder));;
let lcs_body (recurse: dna * dna -> dna) ((s1, s2): dna * dna) : dna = 
  match (s1,s2) with 
  | ([], _) -> []
  | (_, []) -> []
  | (x :: xs, y :: ys) ->
      if Base.eq x y then
        x :: recurse (xs, ys)
      else
        Base.longer_dna_of (recurse (s1, ys)) (recurse (xs, s2))
;;
let fast_lcs (ds : dna * dna) : dna = (AutoMemo.memo lcs_body) ds;;


(* Task 4.5 *)

(* Implement some experiment that shows performance difference
 * between slow_lcs and fast_lcs. (Print your results.)     
 * Explain in a brief comment what your experiment shows.        *)

let print_header () =
  print_string "--------- ----- LCS -----\n";
  print_string "    N     Slow     Fast  \n";
  print_string "--------- ---------------\n"
;;

let print_row n slow fast =
  let space () = print_string "   " in
  let float f = Printf.printf "%6.4f" f in
  let print_slow slow =
    match slow with
    | None   -> print_string "   -  "
    | Some f -> float f
  in
  if n < 10 then print_string " ";
  if n < 100 then print_string " ";
  if n < 1000 then print_string " ";
  if n < 10000 then print_string " ";
  if n < 100000 then print_string " ";
  if n < 1000000 then print_string " ";
  print_int n; space ();
  print_slow slow; space ();
  float fast; space ();
  print_newline()
;;

let experiment (n:int) : unit =
  let bases = ["A"; "G"; "T"; "C"] in
  let rec make_random_dna_string n =
    let () = Random.self_init () in
    let i = Random.int 4 in
    (List.nth bases i) ^ (if n <= 0 then "" else make_random_dna_string (n-1))
  in
  let make_random_dna n = (
    Base.dna_from_string (make_random_dna_string n),
    Base.dna_from_string (make_random_dna_string n)
  ) in
  let slow n =
    if n > 20 then None else Some (time_fun slow_lcs (make_random_dna n))
  in
  let fast n = time_fun fast_lcs (make_random_dna n) in
  print_row n (slow n) (fast n)
;;

(* This generates random DNA strings of differing lengths and runs the two LCS
 * implementations on them. We see that the slow implementation is roughly
 * exponential (with some variation because of randomness), and that the fast
 * implementation is roughly cubic.
 *)
let main () =
  let trials = [(*0;1;2;10;11;12;13;14;15;16;17;18;19;20;*)
  25;30;36;37;38;39;40;50;100;200;400] in
  print_header();
  List.iter experiment trials
;;

(* uncomment this block to run your experiment, 
 * but please do not submit with it uncommented
 *)
(* main ();; *)
