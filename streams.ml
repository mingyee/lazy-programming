(***************** Using the Lazy module ******************)
(* Here we provide an alternate implementation of streams using
   OCaml's lazy module. We recommend that you explore the
   documentation at
   http://caml.inria.fr/pub/docs/manual-ocaml/libref/Lazy.html

   In this portion, you will be reimplementing various functions that
   were defined in class and several more ...
*)

(***************** Using the Num module ******************)
(* This file uses OCaml Num library for arbitrary precision arithmetic.
 * All functions that you write where you might be inclined to use the 
 * simple int type as the type of stream should instead use the num type
 *
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/Num.html
 *
 * To load this file in the OCaml toplevel, you need to load the 
 * Num library. The .ocamlinit file distributed with this code does this 
 * for you. You could, alternately, start Ocaml with nums.cma as an argument: 
 *
 *   % ocaml nums.cma
 *
 * To use ocamlbuild, see the associated Makefile.
 *
 * Some useful operators from the num library include:
 *  (+/) addition on num
 *  (-/) subtraction on nums
 *  ( */ ) multiplication on nums -- watch the spaces or you start a comment
 *  (//) division on nums
 *  ( **/ ) power on nums
 *
 * See the documentation for more.
 *)

open Num;;

open Lazy;;

(* some useful numbers *)
let zero  : num = Int 0
let one   : num = Int 1
let two   : num = Int 2
let three : num = Int 3
let four  : num = Int 4
let five  : num = Int 5

type 'a str = Cons of 'a * 'a stream
and 'a stream = 'a str Lazy.t

(* a stream of ones *)
let rec ones : num stream = 
  lazy (
    Cons (one, ones)
  )




(*>* Problem 2.1.a *>*)
(* Implement the head and tail functions *)
let head (s:'a stream) : 'a =
  match s with
  | lazy (Cons (a, _)) -> a
;;
assert (head @@ ones = one);;
let tail (s:'a stream) : 'a stream =
  match s with
  | lazy (Cons (_, a)) -> a
;;
assert (head @@ tail @@ ones = one);;

(*>* Problem 2.1.b *>*)
(* Implement map *)
let rec map (f:'a -> 'b) (s:'a stream) : 'b stream =
  match s with
  | lazy (Cons (a, b)) -> lazy (Cons (f a, map f b))
;;
assert (head @@ (map ((+/) two) ones) = three);;
assert (head @@ tail @@ (map ((+/) two) ones) = three);;

(*>* Problem 2.1.c *>*)
(* Define the infinite stream of natural numbers *)
(* XXX: Is 0 a natural number? *)
let rec nats : num stream = 
  let rec aux i = lazy (Cons (i, aux (i +/ one))) in
  aux zero
;;
assert (head @@ nats = zero);;
assert (head @@ tail @@ nats = one);;
assert (head @@ tail @@ tail @@ nats = two);;
assert (head @@ tail @@ tail @@ tail @@ nats = three);;
assert (head @@ tail @@ tail @@ tail @@ tail @@ nats = four);;
assert (head @@ tail @@ tail @@ tail @@ tail @@ tail @@ nats = five);;
let evens = map (( */ ) two) nats in
assert (
  (head @@ tail @@ evens) -/ (head @@ evens) = two
  && (head @@ tail @@ tail @@ evens) -/ (head @@ tail @@ evens) = two
);;


(*>* Problem 2.1.d *>*)
(* Write a function nth, which returns the nth element of a
   stream. NOTE: the function nth should be zero-indexed. In other
   words, "nth 0 s" should be equivalent to "head s". *)
let rec nth (n:num) (s:'a stream) : 'a =
  if n </ zero then failwith "nth cannot take negative nums."
  else if n =/ zero then head s
  else nth (pred_num n) (tail s)
;;
assert (
  let s = nats in
  (nth zero s == head s) && (nth one s == head @@ tail @@ s)
);;

(*>* Problem 2.1.e *>*)
(* Now suppose we have two num streams s1 and s2 sorted in ascending
   order. We wish to merge these into a single stream s such that s is
   sorted in ascending order AND s HAS NO DUPLICATES. Implement this
   function TO THE EXTENT POSSIBLE --- in other words, it should return
   a good answer in most cases, but you'll run in to an issue in certain
   cases.  Use the next question to document any failure modes or 
   incompletenesses or unusual aspects of your function.
*)
let rec merge (s1:num stream) (s2:num stream) : num stream =
  let get_head s1 s2 = (
    if      head s1 </ head s2 then (head s1, tail s1, s2)
    else if head s1 =/ head s2 then (head s1, tail s1, tail s2)
    else                            (head s2, s1,      tail s2)
  ) in 
  let rec aux curr s1 s2 = (
    let (h, t1, t2) = get_head s1 s2 in 
    if h >/ curr then lazy (Cons (h, aux h t1 t2))
    else aux curr t1 t2
  ) in
  let (h, t1, t2) = get_head s1 s2 in
  lazy (Cons (h, aux h t1 t2))
;;
let twothree = merge (map (( */) two) nats) (map (( */) three) nats) in
let tests = [(0,0); (1,2); (2,3); (3,4); (4,6); (5,8); (6,9); (7,10); (8,12)] in
List.iter (fun x -> assert (nth (Int (fst x)) twothree = Int (snd x))) tests;;
(*let onesnats = merge ones nats in
Printf.printf "%s\n" (string_of_num @@ head @@ tail @@ onesnats);;*)

(*>* Problem 2.1.f *>*)
(* What kinds of inputs cause your "merge" function above to do something
   bad?  What bad thing happens in these cases?  Answer within the string. *)
let p21f = "Any streams with duplicates will cause our merged stream"
            ^ "to have duplicates. In cases where there are many such"
            ^ "such duplicates, it may take a very long time to find the next"
            ^ "value. Merging ones with anything, for instance, can have"
            ^ "egregiously bad results";;



(*>* Problem 2.2 *>*)

(* Define a type for an infinite spreadsheet full of cells with type 'a. 
 *
 * You are free to use any representation of infinite spreadsheets you choose.
 *
 * Each cell in the spreadsheet will have coordinates (i,j).  You can think
 * of a cell with coordinates (i,j) as the cell inhabiting the ith row and
 * jth column of the spreadsheet.  You can assume that (i,j) are both
 * non-negative.  Indices in the spreadsheet start at 0.

 * Coordinates will be represented using OCaml's arbitrary precision Num
 * library, as in the rest of this file.

 * Such a spreadsheet will need to support the following operations:
 *)

type 'a spread_sheet = 'a stream stream;;

(* you can assume all coordinates given are non-negative *)
type coordinates = num * num ;;

(* a spreadsheet containing all zeros *)
let rec zeros : num spread_sheet = 
  let rec zerostream = lazy (Cons (zero, zerostream)) in
  lazy (Cons (zerostream, zeros))
;;

(* return the element at the (i,j) coordinate in ss *)
let get ((i,j):coordinates) (ss:'a spread_sheet) : 'a = 
  nth j (nth i ss)
;;
assert (get (zero, zero) zeros = zero);;
assert (get (one, zero) zeros = zero);;
assert (get (one, one) zeros = zero);;

(* create a new spreadsheet where the (i,j) element of the spreadsheet
 * contains f i j xij  when xij was the (i,j) element of the input spreadsheet
 *)
let map_all (f:num -> num -> 'a -> 'b) (ss:'a spread_sheet) : 'b spread_sheet = 
  let rec map fn j s = (
    match s with
    | lazy (Cons (a, b)) -> lazy (Cons (fn j a, map fn (succ_num j) s))
  ) in let rec aux fn i j spreadsheet = (
    match spreadsheet with
    | lazy (Cons (a, b)) -> lazy (Cons (
        map (fun j s -> f i j s) j a, aux f (succ_num i) j b
      ))
  ) in aux f zero zero ss
;;

(* create an infinite multiplication table in which every cell contains the
 * product of its indices *)
let multiplication_table = map_all (fun x y _ -> x */ y) zeros;;
assert (get (zero, zero) multiplication_table = zero);; 
assert (get (zero, one) multiplication_table = zero);; 
assert (get (two, two) multiplication_table = four);; 
assert (get (one, one) multiplication_table = one);; 

(* produce a spreadsheet in which cell (i,j) contains the ith element
 * of is and the jth element of js *)
let cross_product (is:'a stream) (js:'b stream) : ('a * 'b) spread_sheet =
  map_all (fun i j _ -> (nth i is, nth j js)) zeros 
;;
let tests = [
  (zero,  zero);  (zero,  one);  (zero,  two);
  (zero,  three); (zero,  four); (zero,  five);
  (one,   zero);  (one,   one);  (one,   two);
  (one,   three); (one,   four); (one,   five);
  (two,   zero);  (two,   one);  (two,   two);
  (two,   three); (two,   four); (two,   five);
  (three, zero);  (three, one);  (three, two);
  (three, three); (three, four); (three, five);
  (four,  zero);  (four,  one);  (four,  two);
  (four,  three); (four,  four); (four,  five);
  (five,  zero);  (five,  one);  (five,  two);
  (five,  three); (five,  four); (five,  five);
] in List.iter (fun x -> assert (
  get x multiplication_table = (fst x) */ (snd x)
  && x = get x (cross_product nats nats)
)) tests;;
